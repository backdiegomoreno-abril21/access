package com.api.access.controllers;

import com.api.access.models.ApiError;
import com.api.access.models.PlacesModel;
import com.api.access.models.UserModel;
import com.api.access.service.CodigoQRService;
import com.api.access.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;


@RestController
public class CodigoQRV1Controller {

    @Autowired
    CodigoQRService codigoQRService;

    @Autowired
    PlaceService placeService;

    /* Create qr-code */
    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/v1/qr-code/{placeName}", produces={"application/json","image/png"})
    public ResponseEntity createQR (@PathVariable String placeName, @RequestBody UserModel user)
    {
        byte[] resultado = null;

        try {
            if(placeService.findByNameIsPresent(placeName)) {
                if (codigoQRService.createQR(user, placeName)) {
                    RestTemplate template = new RestTemplate();

                    resultado = template.getForObject(String.format("https://qrickit.com/api/qr.php?d=%s", user.getUserCode() + "_"
                                    + placeName + "_GO_AHEAD"),
                            byte[].class);

                    //return new ResponseEntity<byte[]>(resultado, HttpStatus.OK);
                    return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(resultado);
                }
            }
            //return new ResponseEntity<>("Lugar no existe", HttpStatus.BAD_REQUEST);
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No existe el lugar", "/v1/qr-code/{placeName}");
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(apiError);
            //return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
        catch(Exception ex) {
            //return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    ex.getMessage(), "/v1/qr-code/{placeName}");
            //return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(apiError);
        }
    }

}
