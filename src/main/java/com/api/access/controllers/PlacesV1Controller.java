package com.api.access.controllers;

import com.api.access.models.ApiError;
import com.api.access.models.PlacesModel;
import com.api.access.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PlacesV1Controller {

    @Autowired
    PlaceService placeService;

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/v1/places", produces = "application/json")
    public ResponseEntity<List<PlacesModel>> getPlaces() {
        List<PlacesModel> list = placeService.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/v1/places/{name}", produces = "application/json")
    public ResponseEntity<PlacesModel> getPlace(@PathVariable String name) {
        PlacesModel placesModel = placeService.findByName(name);
        if(placesModel == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(placesModel, HttpStatus.OK);
    }

    /* Add nuevo place */
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/v1/places", produces="application/json")
    public ResponseEntity createPlace (@RequestBody PlacesModel placeNew)
    {
        try {
            if(placeService.findByNameIsPresent(placeNew.getName())){
                ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                        "Lugar ya existe", "/access/v1/places");
                return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            }
            placeService.save(placeNew);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        catch (Exception e){
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No se puede crear el lugar", "/access/v1/places");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    // Update not idempotent
    @PreAuthorize("hasRole('ADMIN')")
    @PatchMapping(value = "/v1/places/{name}")
    public ResponseEntity updateAvailablePlace(@RequestBody PlacesModel placeUpdate,
                                               @PathVariable String name){
        PlacesModel place = placeService.findByName(name);
        if (place == null) {
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "Lugar no existe", "/access/v1/places/{name}");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
        place.setAvailable(placeUpdate.isAvailable());
        placeService.save(place);
        return new ResponseEntity<>(place, HttpStatus.OK);
    }

    /* Delete id place */
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/v1/places/{name}")
    public ResponseEntity deletePlace(@PathVariable String name)
    {
        try
        {
            placeService.deleteById(name);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "Lugar no existe", "/access/v1/places/{name}");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }


}
