package com.api.access.controllers;

import com.api.access.config.security.JwtTokenUtil;
import com.api.access.models.ApiError;
import com.api.access.models.JwtRequest;
import com.api.access.models.JwtResponse;
import com.api.access.models.RolesModel;
import com.api.access.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
public class AuthenticationController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtServicioEnMemoria;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("v1/authenticate")
    public ResponseEntity createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        String token = null;
        UserDetails userDetails;
        try {
            authenticate(authenticationRequest.getApp(), authenticationRequest.getPassword());
            userDetails = jwtServicioEnMemoria.loadUserByUsername(authenticationRequest.getApp());
            token = jwtTokenUtil.generateToken(userDetails);
        }
        catch (Exception ex) {
            System.out.println(ex);
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    ex.getMessage(), "/access/v1/authenticate");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("token", token);
        return new ResponseEntity<>(new JwtResponse(userDetails.getAuthorities()), headers, HttpStatus.OK);
    }

    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        }
        catch (UsernameNotFoundException ex) {
            System.out.println(ex);
            throw new Exception("App no encontrada");
        }
        catch (DisabledException ex) {
            System.out.println(ex);
            throw new Exception("App deshabilitado");
        }
        catch (BadCredentialsException ex) {
            System.out.println(ex);
            throw new Exception("Credenciales no validas");
        }
        catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("Error Authentication");
        }
    }

}
