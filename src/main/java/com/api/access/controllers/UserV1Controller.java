package com.api.access.controllers;

import com.api.access.models.ApiError;
import com.api.access.models.PlacesModel;
import com.api.access.models.UserModel;
import com.api.access.service.PlaceService;
import com.api.access.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author: Equipo 1
 * @version: 29/04/21
 */
@RestController
public class UserV1Controller {

    @Autowired
    private UserService userService;

    @Autowired
    private PlaceService placeService;

    /**
     * Method getUsers
     * Listara todos los usuarios encontrados x findall
     * No requiere parametro de entrada
     */
    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/v1/users", produces = "application/json")
    public ResponseEntity<List<UserModel>> getUsers(){
        System.out.println("Method ObtenerUsuarios");
        List<UserModel> listaUser = userService.findAll();
        return new ResponseEntity<>(listaUser, HttpStatus.OK);
    }

    /**
     * Method getUserbyId
     * Busqueda de usuario x id
     * @param "idUser" corresponde a codigo de usuario para buscar en DB
     * @return ResponseEntity con los datos del user
     */
    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/v1/users/{userCode}", produces = "application/json")
    public ResponseEntity getUserbyId(@PathVariable String userCode ){
        System.out.println("Method Obtener Usuario by ID");

        UserModel result = userService.findByUser(userCode);
        if(result == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Method createUsers
     * Crear un nuevo usuario
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/v1/users", produces = "application/json")
    public ResponseEntity createUsers(@RequestBody UserModel userNew)
    {
        try {
            if(!userNew.getPlaces().isEmpty()) {
                List<PlacesModel> placesModel = userNew.getPlaces().stream()
                                    .filter(u-> placeService.findByNameIsPresent(u.getName()))
                                    .collect(Collectors.toList());

                if(!placesModel.isEmpty()) {
                    for (PlacesModel places : placesModel) {
                        PlacesModel p = placeService.findByName(places.getName());
                        places.setId(p.getId());
                        places.setDescription(p.getDescription());
                        places.setAvailable(p.isAvailable());
                    }
                    userNew.setPlaces(placesModel);
                }
                else {
                    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                            "No es posible crear usuario, no existen esos lugares", "/access/v1/users");
                    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
                }
            }
            UserModel userModel = userService.save(userNew);

            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        catch (DuplicateKeyException e){
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "Usuario ya existe", "/access/v1/users");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No es posible crear usuario", "/access/v1/users");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method updateUserData
     * modificar parametros de un usuario
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PatchMapping(value="/v1/users/{userCode}", produces = "application/json")
    public ResponseEntity updateUserData(@PathVariable String userCode, @RequestBody UserModel userModelUp)
    {
        Optional<UserModel> usuario = userService.findById(userService.findByUser(userCode).getId());
        try {
            if(usuario.isPresent()){
                UserModel modifyUser = usuario.get();
                modifyUser.setName(userModelUp.name);
                modifyUser.setLastName(userModelUp.lastName);
                UserModel userUpdate = userService.save(usuario.get());
                return new ResponseEntity<>(HttpStatus.OK);
            }
            else{
                ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                        "Usuario No Encontrado", "/access/v1/users");
                return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /* Delete id place */
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/v1/users/{userCode}")
    public ResponseEntity deleteUser(@PathVariable String userCode)
    {
        try
        {
            userService.deleteById(userCode);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No se ha encontrado el usuario", "/access/v1/users/{userCode}");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method createPlaceToUser
     * Crear un nuevo lugar a un usuario
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/v1/users/{userCode}/places", produces = "application/json")
    public ResponseEntity createPlaceToUser(@PathVariable String userCode, @RequestBody List<PlacesModel> placeModel)
    {
        try {
            UserModel usuario = userService.findByUser(userCode);
            if (usuario != null) {
                if (!placeModel.isEmpty()) {
                    if (!placeService.findByPlaceToUser(placeModel)) {
                        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                                "No existen esos lugares", "/access/v1/users/{userId}/places");
                        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
                    }
                } else {
                    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                            "No esta adjuntado lugares", "/access/v1/users/{userId}/places");
                    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
                }
            } else {
                ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                        "No es posible crear lugar, no existe el usuario", "/access/v1/users/{userId}/places");
                return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            }

            if (usuario.getPlaces()
                    .stream()
                    .anyMatch(placeModel::contains)) {
                ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                        "Lugar ya esta relacionado al usuario", "/access/v1/users/{userId}/places");
                return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            }

            for (PlacesModel place: placeModel) {
                usuario.getPlaces().add(place);
            }
            UserModel userModel = userService.save(usuario);

            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e){
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No es posible asignar el lugar al usuario", "/access/v1/users/{userId}/places");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method getPlacesToUser
     * Obtiene todos los lugares asociados a un usuario
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/v1/users/{userCode}/places", produces = "application/json")
    public ResponseEntity getPlacesToUser(@PathVariable String userCode)
    {
        try {
            UserModel usuario = userService.findByUser(userCode);
            if (usuario != null) {
                return (usuario.getPlaces() != null) ? new ResponseEntity<>(usuario.getPlaces(), HttpStatus.OK)
                                                    : new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                        "No existe el usuario",
                        "/access/v1/users/{userId}/places/{name}");
                return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No es posible obtener los sitios", "/access/v1/users/{userId}/places/{name}");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method updateAvailablePlaceToUser
     * Actualizar la disponiblidad a un lugar
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/v1/users/{userCode}/places/{name}", produces = "application/json")
    public ResponseEntity updateAvailablePlaceToUser(@PathVariable String userCode,
                                                     @PathVariable String name,
                                                     @RequestBody PlacesModel placeModel)
    {
        try {
            UserModel usuario = userService.findByUser(userCode);
            if (usuario != null) {
                PlacesModel place = placeService.findByName(name);
                if (place == null) {
                    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                            "No existe ese lugar", "/access/v1/users/{userId}/places/{name}");
                    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
                } else {
                    for (PlacesModel pl: usuario.getPlaces()) {
                        if(pl.getName().equals(name)) {
                            pl.setAvailable(placeModel.available);
                            userService.save(usuario);
                            return new ResponseEntity<>(HttpStatus.OK);
                        }
                    }
                    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                            "Ese lugar no esta asociado al usuario",
                            "/access/v1/users/{userId}/places/{name}");
                    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
                }
            } else {
                ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                        "No es posible actualizar la disponiblidad al lugar, no existe el usuario",
                        "/access/v1/users/{userId}/places/{name}");
                return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No es posible actualizar acceso al lugar", "/access/v1/users/{userId}/places/{name}");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method deletePlaceToUser
     * Elimina un lugar asociado a un cliente
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/v1/users/{userCode}/places/{name}", produces = "application/json")
    public ResponseEntity deletePlaceToUser(@PathVariable String userCode, @PathVariable String name)
    {
        try {
            UserModel usuario = userService.findByUser(userCode);
            if (usuario != null) {
                if (usuario.getPlaces() == null || usuario.getPlaces().isEmpty()) {
                    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                            "No existen lugares asociados", "/access/v1/users/{userId}/places/{name}");
                    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
                } else {
                    if(usuario.getPlaces().removeIf(s->s.getName().equals(name))) {
                        userService.save(usuario);
                        return new ResponseEntity<>(HttpStatus.OK);
                    }
                    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                            "Ese lugar no esta asociado al usuario",
                            "/access/v1/users/{userId}/places/{name}");
                    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
                }
            } else {
                ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                        "No es posible eliminar el lugar, no existe el usuario",
                        "/access/v1/users/{userId}/places/{name}");
                return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
                    "No es posible eliminar al lugar al usuario", "/access/v1/users/{userId}/places/{name}");
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

}
