package com.api.access.models;

public class RolesModel {

    private String rolName;

    public RolesModel() {
    }

    public RolesModel(String rolName) {
        this.rolName = rolName;
    }

    public String getRolName() {
        return rolName;
    }

    public void setRolName(String rolName) {
        this.rolName = rolName;
    }

    @Override
    public String toString() {
        return "RolesModel{" +
                "rolName='" + rolName + '\'' +
                '}';
    }
}
