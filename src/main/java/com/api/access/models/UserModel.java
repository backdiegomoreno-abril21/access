package com.api.access.models;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("user")
public class UserModel {

    public String id;
    @Indexed(unique = true)
    public String userCode;
    public String name;
    public String lastName;
    public String password;
    public WorkdayModel workday;
    public List<PlacesModel> places;

    public UserModel() {
    }

    public UserModel(String id, String userCode, String name, String lastName, String password, WorkdayModel workdayModel,
                     List<PlacesModel> places) {
        this.id = id;
        this.userCode = userCode;
        this.name = name;
        this.lastName = lastName;
        this.password = password;
        this.workday = workdayModel;
        this.places = places;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public WorkdayModel getWorkday() {
        return workday;
    }

    public void setWorkday(WorkdayModel workdayModel) {
        this.workday = workdayModel;
    }

    public List<PlacesModel> getPlaces() {
        return places;
    }

    public void setPlaces(List<PlacesModel> places) {
        this.places = places;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id='" + id + '\'' +
                ", userCode='" + userCode + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", workdayModel=" + workday +
                ", places=" + places +
                '}';
    }
}
