package com.api.access.models;

import java.util.Date;

public class WorkdayModel {

    public Date starTime;
    public Date endTime;

    public WorkdayModel() {
    }

    public WorkdayModel(Date starTime, Date endTime) {
        this.starTime = starTime;
        this.endTime = endTime;
    }

    public Date getStarTime() {
        return starTime;
    }

    public void setStarTime(Date starTime) {
        this.starTime = starTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "WorkdayModel{" +
                "starTime=" + starTime +
                ", endTime=" + endTime +
                '}';
    }
}
