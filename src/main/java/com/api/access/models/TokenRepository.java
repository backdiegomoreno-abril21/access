package com.api.access.models;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends MongoRepository<JwtRequest, String> {

    @Query("{'app':?0}")
    public JwtRequest findByApp(String app);

}