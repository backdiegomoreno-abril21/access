package com.api.access.models;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document("granting")
public class JwtRequest  implements Serializable {

    private String id;
    private String app;
    private String password;
    private List<RolesModel> roles;

    public JwtRequest() {
    }

    public JwtRequest(String id, String app, String password, List<RolesModel> roles) {
        this.setId(id);
        this.setApp(app);
        this.setPassword(password);
        this.setRoles(roles);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<RolesModel> getRoles() {
        return roles;
    }

    public void setRoles(List<RolesModel> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "JwtRequest{" +
                "id='" + id + '\'' +
                ", app='" + app + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                '}';
    }
}
