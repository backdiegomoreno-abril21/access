package com.api.access.models;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class JwtResponse  implements Serializable {

    private final Collection<? extends GrantedAuthority> authorities;

    public JwtResponse(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
}
