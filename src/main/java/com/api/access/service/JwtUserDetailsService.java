package com.api.access.service;

import com.api.access.models.JwtRequest;
import com.api.access.models.TokenRepository;
import com.api.access.models.UserModel;
import com.api.access.models.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    private final String password = "$2y$12$DxmQvHrtKKUB/lsy9OU8QOuuc0KwiU22js3E8hxbBH0GqSgE6nvne";

    @Override
    public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {
        System.out.println("loadUserByUsername " + user);
        JwtRequest jwtRequest = validateApp(user);
        if(jwtRequest != null) {
            List<GrantedAuthority> authorities =
                    jwtRequest.getRoles()
                            .stream()
                            .map(rol -> new SimpleGrantedAuthority(rol.getRolName()))
                            .collect(Collectors.toList());
            System.out.println("authorities " + authorities);
            return new User(user, passwordEncoder.encode(jwtRequest.getPassword()), authorities);
        }
        else {
            return null;
        }
    }

    /* Validación DB UserModel */
    public JwtRequest validateApp (String userCode) {
        JwtRequest grantingModel = null;
        grantingModel = tokenRepository.findByApp(userCode);
        System.out.println("grantingModel " + grantingModel);
        return grantingModel;
    }

}
