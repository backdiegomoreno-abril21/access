package com.api.access.service;

import com.api.access.models.PlaceRepository;
import com.api.access.models.PlacesModel;
import com.api.access.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlaceService {

    @Autowired
    PlaceRepository placeRepository;

    // READ Documents
    public List<PlacesModel> findAll() {
        return placeRepository.findAll();
    }

    // READ One Document
    public Optional<PlacesModel> findById(String id) {
        return placeRepository.findById(id);
    }

    // READ One Document
    public PlacesModel findByName(String name) {
        return placeRepository.findByName(name);
    }

    // READ One Document
    public boolean findByNameIsPresent(String name) {
        if (placeRepository.findByName(name) == null) {
            return false;
        }
        return true;
    }

    // CREATE Document
    public PlacesModel save(PlacesModel placesModel) {
        return placeRepository.save(placesModel);
    }

    // DELETE Document
    public void deleteById(String name) {
        PlacesModel placesModel = this.findByName(name);
        placeRepository.deleteById(placesModel.getId());
    }

    // READ place to user
    public boolean findByPlaceToUser(List<PlacesModel> places) {

        List<PlacesModel> placesModel = places.stream()
                .filter(u -> this.findByNameIsPresent(u.getName()))
                .collect(Collectors.toList());

        if (!placesModel.isEmpty()) {
            for (PlacesModel pl : placesModel) {
                PlacesModel p = this.findByName(pl.getName());
                pl.setId(p.getId());
                pl.setDescription(p.getDescription());
            }
            places = placesModel;
        }
        else {
            return false;
        }
        return true;

    }
    
}
