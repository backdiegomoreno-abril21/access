package com.api.access.service;

import com.api.access.models.PlaceRepository;
import com.api.access.models.PlacesModel;
import com.api.access.models.UserModel;
import com.api.access.models.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    // READ Collections
    public List<UserModel> findAll() {
        return userRepository.findAll();
    }

    // READ instance
    public Optional<UserModel> findById(String  id) {
        return userRepository.findById(id);
    }

    // READ instance
    public UserModel findByUser(String  user) {
        return userRepository.findByUser(user);
    }

    // CREATE
    public UserModel save(UserModel entity) {
        return userRepository.save(entity);
    }

    // DELETE Document
    public void deleteById(String name) {
        UserModel user = this.findByUser(name);
        userRepository.deleteById(user.getId());
    }

}
