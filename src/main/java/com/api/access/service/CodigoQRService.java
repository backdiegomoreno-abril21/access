package com.api.access.service;

import com.api.access.models.PlaceRepository;
import com.api.access.models.PlacesModel;
import com.api.access.models.UserModel;
import com.api.access.models.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CodigoQRService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PlaceRepository placeRepository;

    public boolean createQR (UserModel userModel, String placeName) throws Exception {

        UserModel userAux = userRepository.findByUser(userModel.getUserCode());
        System.out.println("Validate " + userModel);
        System.out.println("userAux " + userAux);
        if(userAux != null) {
            System.out.println("  " + userAux.getPassword() + " " + userModel.getPassword());
            if (userAux != null && userAux.getPlaces() != null && (userAux.getPassword().equals(userModel.getPassword()))) {
                PlacesModel placesModel = placeRepository.findByName(placeName);

                for (PlacesModel place : userAux.getPlaces()) {
                        System.out.println("Lugar scan " + place);
                        if (placesModel != null && (place.getName().equals(placesModel.getName()))
                                                && (place.isAvailable() == placesModel.isAvailable())) {
                            System.out.println("Lugar disponible " + place);
                            return true;
                        }
                }
                throw new Exception("Ningún lugar disponible");
            }
            else {
                throw new Exception("Credenciales erroneas");
            }
        } else {
            throw new Exception("Usuario no existe");
        }
    }

}
