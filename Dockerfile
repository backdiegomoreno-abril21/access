FROM openjdk:15-jdk-alpine
COPY target/access-0.0.1.jar api.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "/api.jar"]